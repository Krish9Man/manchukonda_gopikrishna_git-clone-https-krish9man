/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.*;
/**
 *
 * @author gopikrishnamanchukonda
 */
public class CarsCatalog {
    private ArrayList <Cars> CarsCatalog;
    public CarsCatalog(){
        CarsCatalog = new ArrayList<Cars>();
        //String company, boolean maintainExpdate, int yearOfManufacture, boolean isThataUber, String color, String model, String serialno, boolean statusOfCar, int seating_cap, String city, String currentLocation
        Cars car1 = new Cars("Tyota",true,1995,true,"Red","Corola","1234",true,"Boston","Huntington",2,6);
        Cars car2 = new Cars("Honda",false,1996,true,"Red","Civic","234",false,"Boston","Malden",2,6);
        Cars car3 = new Cars("Merc",true,2019,false,"Red","C3","1322",true,"Boston","Roxberry",2,8);
        Cars car4 = new Cars("Tyota",false,2017,true,"Red","RAV4","12232",true,"Iowa","Ave",4,7);
        Cars car5 = new Cars("Honda",true,2014,true,"Red","Fit","2323",false,"Boston","DownTown",2,4);
        Cars car6 = new Cars("Audi",true,2009,false,"Red","A4","12564",true,"Boston","Ruggles",2,8);
        Cars car7 = new Cars("BMW",false,2018,false,"Red","E3","45645",false,"Boston","Mass ave",2,6);
        Cars car8 = new Cars("Honda",true,2017,true,"Red","Civic","14353",true,"Boston","Cambridge",2,8);
        Cars car9 = new Cars("Lexus",true,2015,false,"Red","Con","1454",true,"Boston","Assembly Row",2,6);
       CarsCatalog.add(car1);
       CarsCatalog.add(car2);
       CarsCatalog.add(car3);
       CarsCatalog.add(car4);
       CarsCatalog.add(car5);
       CarsCatalog.add(car6);
        CarsCatalog.add(car7);
        CarsCatalog.add(car8);
       CarsCatalog.add(car9);
       
        /*
        Car car1 = new Car(true, "Ferrari", 2019, 1, 2, 1,"Red One", "Boston", true);
        Car car2 = new Car(true, "BMW", 2019, 1, 4, 2, "X1", "New York", true);
        Car car3 = new Car(true, "Toyota", 2018, 1, 4, 3, "T1", "Boston", true);
        Car car4 = new Car(true, "GM", 2018, 1, 4, 4, "G1", "New York", true);
        Car car5 = new Car(true, "Toyota", 2017, 1, 4, 5, "T2", "Chicago", true);
        Car car6 = new Car(true, "GM", 2017, 1, 4, 6, "G2", "Chicago", true);
        Car car7 = new Car(true, "Ferrari", 2016, 1, 4, 7, "", "Seattle", true);
        Car car8 = new Car(true, "BMW", 2016, 1, 4, 8, "X2", "Seattle", true);
        Car car9 = new Car(true, "Toyota", 2019, 1, 4, 9, "T3", "Austin", true);
        Car car10 = new Car(true, "GM", 2019, 1, 4, 10, "G3", "Austin", true);
        
        */
    }

    public ArrayList<Cars> getCarsCatalog() {
        return CarsCatalog;
    }
    

    public void setCarsCatalog(ArrayList<Cars> CarsCatalog){
        this.CarsCatalog = CarsCatalog;
        
    }
    public void addCars(Cars c){
        
        CarsCatalog.add(c);
     
    }
    
    public void removeCars(Cars c){
        CarsCatalog.remove(c);
        
    }
}
