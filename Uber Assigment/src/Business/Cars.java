/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.LocalDate;  

/**
 *
 * @author gopikrishnamanchukonda
 */
public class Cars {
    private String company;
    //private String price;
    private boolean maintainExpdate;
    private int yearOfManufacture;
    private boolean isThataUber;
    //private String description;
    private String color;
    private String model;
    private String serialno;
    private boolean statusOfCar;//is that availble for ride;
    //private String dimensions;
    //private String fuel_type;
    //private String transmission;
    //private String engine;
    //private String milage;
    //private String wheel_base;
    //private String weight;       
    private int seating_cap;
    private int max_seat;
    private int min_seat;
    //private Date lastUpdated = new Date(119,00, 21);
    
//    date1 = dateformat.parse("27/09/2012");
    //new SimpleDateFormat( "yyyyMMdd" ).parse( "20100520" )
   // Date date = new Date(0);
    //DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
    //String date1 = "2015-12-01";  
    LocalDate date1 = LocalDate.of(2017, 1, 13);

    public LocalDate getDate1() {
        return date1;
    }

    public void setDate1(LocalDate date1) {
        this.date1 = date1;
    }


    public int getMax_seat() {
        return max_seat;
    }

    public void setMax_seat(int max_seat) {
        this.max_seat = max_seat;
    }

    public int getMin_seat() {
        return min_seat;
    }

    public void setMin_seat(int min_seat) {
        this.min_seat = min_seat;
    }

    public boolean isMaintainExpdate() {
        return maintainExpdate;
    }

    //private String fuel_cap;
    // private String air_bags_num;
    //private String top_speed;
    public void setMaintainExpdate(boolean maintainExpdate) {
        this.maintainExpdate = maintainExpdate;
    }
    //private String auto_piolet;
    //private String heater_ac;
    private String city;
    private String currentLocation;

    public Cars(String company, boolean maintainExpdate, int yearOfManufacture, boolean isThataUber, String color, String model, String serialno, boolean statusOfCar, String city, String currentLocation,int min_seats,int max_seats) {
        //Cars ca1 = new Cars("Tyota","true",1995,false,"its a car","Red","Corola",1234,true,4,"Boston","Huntington");
        //last thing is so change 
        this.company = company;
        this.maintainExpdate = maintainExpdate;
        this.yearOfManufacture = yearOfManufacture;
        this.isThataUber = isThataUber;
       // this.description = description;
        this.color = color;
        this.model = model;
        this.serialno = serialno;
        this.statusOfCar = statusOfCar;
        //this.seating_cap = seating_cap;
        this.city = city;
        this.currentLocation = currentLocation;
        this.min_seat= min_seats;
        this.max_seat = max_seats;
        
    }
    

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

   

   

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public boolean isIsThataUber() {
        return isThataUber;
    }

    public void setIsThataUber(boolean isThataUber) {
        this.isThataUber = isThataUber;
    }

    /*public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }*/

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public boolean isStatusOfCar() {
        return statusOfCar;
    }

    public void setStatusOfCar(boolean statusOfCar) {
        this.statusOfCar = statusOfCar;
    }

    public int getSeating_cap() {
        return seating_cap;
    }

    public void setSeating_cap(int seating_cap) {
        this.seating_cap = seating_cap;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }
   @Override
    public String toString(){
        return this.company;
    }

    
}
