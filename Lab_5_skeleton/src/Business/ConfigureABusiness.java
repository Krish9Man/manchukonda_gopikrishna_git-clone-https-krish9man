/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ran
 */
public class ConfigureABusiness {
    
    public static Business configure(){
        // Three roles: LabAssistant, Doctor, Admin
        
        Business business = Business.getInstance();
        
        // create admin organization
        AdminOrganization adminOrganization = new AdminOrganization();
        DoctorOrganization doctorOrganization = new DoctorOrganization();
        LabOrganization labOrganization = new LabOrganization();
        
        // add created admin organization into business
        business.getOrganizationDirectory().getOrganizationList().add(adminOrganization);
        business.getOrganizationDirectory().getOrganizationList().add(doctorOrganization);
        business.getOrganizationDirectory().getOrganizationList().add(labOrganization);
        
        // create new employee
        Employee employee = new Employee();
        employee.setName("Admin XYZ");
        
        Employee doctor = new Employee();
        doctor.setName("Sheroff");
        
        Employee lab = new Employee();
        lab.setName("Buky");
        
        
        // create new account
        
        UserAccount account = new UserAccount();
        account.setUsername("admin");
        account.setPassword("admin");
        account.setRole("Admin");
        account.setEmployee(employee);
        
        UserAccount accountDoctor = new UserAccount();
        accountDoctor.setUsername("doctor");
        accountDoctor.setPassword("doctor");
        accountDoctor.setRole("Doctor");
        accountDoctor.setEmployee(doctor);
        
        UserAccount accountLab = new UserAccount();
        accountLab.setUsername("lab");
        accountLab.setPassword("lab");
        accountLab.setRole("Lab Assistant");
        accountLab.setEmployee(lab);
        
        
        // add the employee into admin organization
        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee);
        doctorOrganization.getEmployeeDirectory().getEmployeeList().add(doctor);
        labOrganization.getEmployeeDirectory().getEmployeeList().add(lab);
        
        // add the account into admin organization
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(account);
        doctorOrganization.getUserAccountDirectory().getUserAccountList().add(accountDoctor);
        labOrganization.getUserAccountDirectory().getUserAccountList().add(accountLab);
        
        return business;
    }
    
}
