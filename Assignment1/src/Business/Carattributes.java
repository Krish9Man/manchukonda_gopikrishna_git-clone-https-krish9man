/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class Carattributes {
    private String name;
    private String price;
    private String description;
    private String color;
    private String model;
    private String serialno;
    private String dimensions;
    private String fuel_type;
    private String transmission;
    private String engine;
    private String milage;
    private String wheel_base;
    private String weight;       
    private String seating_cap;
    private String fuel_cap;
    private String air_bags_num;
    private String top_speed;
    private String auto_piolet;
    private String heater_ac;
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getFuel_type() {
        return fuel_type;
    }

    public void setFuel_type(String fuel_type) {
        this.fuel_type = fuel_type;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getMilage() {
        return milage;
    }

    public void setMilage(String milage) {
        this.milage = milage;
    }

    public String getWheel_base() {
        return wheel_base;
    }

    public void setWheel_base(String wheel_base) {
        this.wheel_base = wheel_base;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSeating_cap() {
        return seating_cap;
    }

    public void setSeating_cap(String seating_cap) {
        this.seating_cap = seating_cap;
    }

    public String getFuel_cap() {
        return fuel_cap;
    }

    public void setFuel_cap(String fuel_cap) {
        this.fuel_cap = fuel_cap;
    }

    public String getAir_bags_num() {
        return air_bags_num;
    }

    public void setAir_bags_num(String air_bags_num) {
        this.air_bags_num = air_bags_num;
    }

    public String getTop_speed() {
        return top_speed;
    }

    public void setTop_speed(String top_speed) {
        this.top_speed = top_speed;
    }

    public String getAuto_piolet() {
        return auto_piolet;
    }

    public void setAuto_piolet(String auto_piolet) {
        this.auto_piolet = auto_piolet;
    }

    public String getHeater_ac() {
        return heater_ac;
    }

    public void setHeater_ac(String heater_ac) {
        this.heater_ac = heater_ac;
    }

}
